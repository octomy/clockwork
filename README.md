[![pipeline status](https://gitlab.com/octomy/clockwork/badges/production/pipeline.svg)](https://gitlab.com/octomy/clockwork/-/commits/production)
# About clockwork

<img src="https://gitlab.com/octomy/clockwork/-/raw/production/design/logo-1024.png" width="20%"/>

Clockwork is a cheduler for tasks tightly linked to [the batch project](https://gitlab.com/octomy/batch)

- Clockwork is [available on gitlab](https://gitlab.com/octomy/clockwork).
- Clockwork is [available in PyPI](https://pypi.org/project/clockwork/).

```shell
# Clone git repository
git clone git@gitlab.com:octomy/clockwork.git

```

```shell
# Install clockwork into your current Python environment
pip install octomy-clockwork

```
